
import { NativeModules } from 'react-native';
import UnifiedBanner from './libs/GDTUnifiedBanner';
import NativeExpress from './libs/GDTNativeExpress';
import Splash from './libs/GDTSplash';
import VivoBanner from './libs/VivoBanner';
import VivoNativeExpress from './libs/VivoNativeExpress';
import VivoSplash from './libs/VivoSplash';
import HMSSplash from './libs/HMSSplash'
const Module = NativeModules.GDTModule;

const GDT = {
	HMSSplash,
	Splash,
    UnifiedBanner,
    NativeExpress,
    VivoBanner,
    VivoNativeExpress,
    VivoSplash,
    Module
};
module.exports = GDT;