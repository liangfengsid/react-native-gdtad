package cn.cnlee.commons.gdt;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.common.MapBuilder;
import com.facebook.react.uimanager.SimpleViewManager;
import com.facebook.react.uimanager.ThemedReactContext;
import com.facebook.react.uimanager.annotations.ReactProp;
import com.facebook.react.uimanager.events.RCTEventEmitter;
import com.google.gson.Gson;
import com.vivo.ad.model.AdError;
import com.vivo.ad.splash.SplashAdListener;

import java.util.Map;

import javax.annotation.Nullable;

import cn.cnlee.commons.gdt.view.VivoSplash;

public class VivoSplashViewManager extends SimpleViewManager {

    private static final String TAG = "VivoSplash";

    // 重写getName()方法, 返回的字符串就是RN中使用该组件的名称
    @Override
    public String getName() {
        return TAG;
    }
//
//    @Override
//    public void onADDismissed() {
//        Log.i(TAG, "onADDismissed");
//        mEventEmitter.receiveEvent(mContainer.getId(), Events.EVENT_DISMISSED.toString(), null);
//    }
//
//    @Override
//    public void onNoAD(AdError adError) {
//        Log.i(TAG, "onNoAD: eCode=" + adError.getErrorCode() + ",eMsg=" + adError.getErrorMsg());
//        final WritableMap event = Arguments.createMap();
//        event.putString("error", new Gson().toJson(adError));
//        long alreadyDelayMills = System.currentTimeMillis() - fetchSplashADTime;//从拉广告开始到onNoAD已经消耗了多少时间
//        //为防止加载广告失败后立刻跳离开屏可能造成的视觉上类似于"闪退"的情况，根据设置的minSplashTimeWhenNoAD 计算出还需要延时多久
//        long shouldDelayMills = alreadyDelayMills > minSplashTimeWhenNoAD ? 0 : minSplashTimeWhenNoAD - alreadyDelayMills;
//        Log.i(TAG, "shouldDelayMills: " + shouldDelayMills);
//        handler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                mEventEmitter.receiveEvent(mContainer.getId(), Events.EVENT_FAIL_TO_RECEIVED.toString(), event);
//            }
//        }, shouldDelayMills);
//    }
//
//    @Override
//    public void onADPresent() {
//        Log.i(TAG, "onADPresent");
//        if (mSplash != null) {
//            mSplash.requestLayout();
//        }
//        mEventEmitter.receiveEvent(mContainer.getId(), Events.EVENT_PRESENT.toString(), null);
//    }
//
//    @Override
//    public void onADClicked() {
//        Log.i(TAG, "onADClicked");
//        mEventEmitter.receiveEvent(mContainer.getId(), Events.EVENT_ON_CLICK.toString(), null);
//
//    }
//
//    public enum Events {
//        EVENT_FAIL_TO_RECEIVED("onFailToReceived"),
//        EVENT_PRESENT("onPresent"),
//        EVENT_DISMISSED("onDismissed"),
//        EVENT_ON_CLICK("onClicked");
//
//        private final String mName;
//
//        Events(final String name) {
//            mName = name;
//        }
//
//        @Override
//        public String toString() {
//            return mName;
//        }
//    }
//
//    private FrameLayout mContainer;
//    private RCTEventEmitter mEventEmitter;
//    private ThemedReactContext mThemedReactContext;
//    private VivoSplash mSplash;
//
//    private int minSplashTimeWhenNoAD = 2000;
//    private long fetchSplashADTime = System.currentTimeMillis();
//    private Handler handler = new Handler(Looper.getMainLooper());
//
    @Override
    protected View createViewInstance(ThemedReactContext reactContext) {
//        mThemedReactContext = reactContext;
//        mEventEmitter = reactContext.getJSModule(RCTEventEmitter.class);
//        FrameLayout viewGroup = new FrameLayout(reactContext);
//        mContainer = viewGroup;
//        return viewGroup;
        return null;
    }

//    @Nullable
//    @Override
//    public Map<String, Object> getExportedCustomDirectEventTypeConstants() {
//        MapBuilder.Builder<String, Object> builder = MapBuilder.builder();
//        for (Events event : Events.values()) {
//            builder.put(event.toString(), MapBuilder.of("registrationName", event.toString()));
//        }
//        return builder.build();
//    }
//
//    // 其中，可以通过@ReactProp（或@ReactPropGroup）注解来导出属性的设置方法。
//    // 该方法有两个参数，第一个参数是泛型View的实例对象，第二个参数是要设置的属性值。
//    // 方法的返回值类型必须为void，而且访问控制必须被声明为public。
//    // 组件的每一个属性的设置都会调用Java层被对应ReactProp注解的方法
//    @ReactProp(name = "appInfo")
//    public void setAppInfo(FrameLayout view, ReadableMap appInfo) {
//        Log.i("VivoSplash", appInfo.toString());
//        String posID = appInfo.getString("posId");
//        String appTitle = appInfo.hasKey("appTitle")?appInfo.getString("appTitle"):"";
//        String appDesc = appInfo.hasKey("appDesc")?appInfo.getString("appDesc"):"";
//        if (posID != null && posID != "") {
//            if (mThemedReactContext.hasCurrentActivity()) {
//                VivoSplash splash = new VivoSplash(mThemedReactContext.getCurrentActivity(), appTitle, appDesc, posID, this);
//                mSplash = splash;
//                view.addView(splash);
//            }
//        }
//    }
}
