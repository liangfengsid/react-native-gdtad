package cn.cnlee.commons.gdt.view;

import android.app.Activity;
import android.content.Context;
import android.widget.RelativeLayout;
import android.annotation.TargetApi;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import androidx.annotation.NonNull;

import com.huawei.hms.ads.AdParam;
import com.huawei.hms.ads.AudioFocusType;
import com.huawei.hms.ads.splash.SplashAdDisplayListener;
import com.huawei.hms.ads.splash.SplashView;

import cn.cnlee.commons.gdt.R;
import cn.cnlee.commons.gdt.view.dialog.ProtocolDialog;
import cn.cnlee.commons.gdt.util.AdsConstant;


public class HMSSplash extends RelativeLayout {
    private static final String TAG = "HMSSplash";

    private Runnable mLayoutRunnable;

    /**
     * 超时消息回调handler，用于保护开屏异常，保证一段时间后能够进入主界面
     */
    private Handler timeoutHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(@NonNull Message msg) {
            jump();
            return false;
        }
    });

    private Handler protocolHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(@NonNull Message msg) {
            if (((Activity) ctx).hasWindowFocus()) {
                switch (msg.what) {
                    case PROTOCOL_MSG_TYPE:
                        showPrivacyDialog();
                        break;
                }
            }
            return false;
        }
    });

    /** 广告展示超时时间：单位毫秒 */
    private static final int AD_TIMEOUT = 5000;

    /** 广告超时消息标记 */
    private static final int MSG_AD_TIMEOUT = 1001;

    private static final int PROTOCOL_MSG_TYPE = 100;

    private static final int MSG_DELAY_MS = 1000;

    /** 默认slogan资源ID，平板和手机使用的资源不同 */
    private int defaultSlogan = R.drawable.default_slogan;

    private Context ctx;
    private String posID;
    private SplashView.SplashAdLoadListener loadListener;
    private SplashAdDisplayListener displayListener;
    private SplashView splashAdView;

    public HMSSplash(Context context, String posID, SplashView.SplashAdLoadListener loadListener, SplashAdDisplayListener displayListener) {
        super(context);
        // 把布局加载到这个View里面
        this.ctx = context;
        this.posID = posID;
        this.loadListener = loadListener;
        this.displayListener = displayListener;
        inflate(context, R.layout.layout_hms_splash, this);

        // 锁定屏幕方向
        ((Activity) context).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LOCKED);

        // sendMessage(PROTOCOL_MSG_TYPE, MSG_DELAY_MS);
        initView(posID, loadListener, displayListener);
    }


    /**
     * 初始化View
     */
    private void initView(String posID, SplashView.SplashAdLoadListener loadListener, SplashAdDisplayListener displayListener) {
        AdParam adParam = new AdParam.Builder().build();

        splashAdView = findViewById(R.id.hms_splash_ad_view);
        splashAdView.setAdDisplayListener(displayListener);
        splashAdView.setSloganResId(defaultSlogan); // slogan默认图片

        splashAdView.setLogoResId(R.drawable.ic_launcher); // 全屏开屏广告，在左上角展示的应用图标
        splashAdView.setMediaNameResId(R.string.app_name); // 全屏开屏广告，在左上角展示的应用名称
        // Set the audio focus type for a video splash ad.
        splashAdView.setAudioFocusType(AudioFocusType.NOT_GAIN_AUDIO_FOCUS_WHEN_MUTE);

        splashAdView.load(posID, ActivityInfo.SCREEN_ORIENTATION_PORTRAIT, adParam, loadListener);

        // 发送延迟消息,用来处理广告超时时能够返回
        timeoutHandler.removeMessages(MSG_AD_TIMEOUT);
        timeoutHandler.sendEmptyMessageDelayed(MSG_AD_TIMEOUT, AD_TIMEOUT);
    }

    @Override
    public void requestLayout() {
        super.requestLayout();
        if (mLayoutRunnable != null) {
            removeCallbacks(mLayoutRunnable);
        }
        mLayoutRunnable = new Runnable() {
            @Override
            public void run() {
                measure(
                        MeasureSpec.makeMeasureSpec(getWidth(), MeasureSpec.EXACTLY),
                        MeasureSpec.makeMeasureSpec(getHeight(), MeasureSpec.EXACTLY));
                layout(getLeft(), getTop(), getRight(), getBottom());
            }
        };
        post(mLayoutRunnable);
    }

    private void jump() {
        Log.d(TAG, "hms jump");
        this.loadListener.onAdDismissed();
    }

    /**
     * 是否是多窗口模式，多窗口模式影响开屏效果，不支持开屏广告。
     *
     * @return
     */
    @TargetApi(24)
    private boolean isMultiWin() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N && ((Activity) this.getContext()).isInMultiWindowMode()) {
            return true;
        }
        return false;
    }

    /**
     * Display the app privacy protocol dialog box.
     */
    private void showPrivacyDialog() {
        // If a user does not agree to the service agreement, the service agreement dialog is displayed.
        if (getPreferences(AdsConstant.SP_PROTOCOL_KEY, AdsConstant.DEFAULT_SP_PROTOCOL_VALUE) == 0) {
            Log.i(TAG, "Show protocol dialog.");
            ProtocolDialog dialog = new ProtocolDialog(ctx);
            dialog.setCallback(new ProtocolDialog.ProtocolDialogCallback() {
                @Override
                public void agree() {
                    initView(posID, loadListener, displayListener);
                }

                @Override
                public void cancel() {
                    // if the user selects the CANCEL button, exit application.
                    ((Activity)ctx).finish();
                }
            });
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        } else {
            initView(posID, loadListener, displayListener);
        }
    }


    private int getPreferences(String key, int defValue) {
        SharedPreferences preferences = ctx.getSharedPreferences(AdsConstant.SP_NAME, Context.MODE_PRIVATE);
        int value = preferences.getInt(key, defValue);
        Log.i(TAG, "Key:" + key + ", Preference value is: " + value);
        return value;
    }

    private void sendMessage(int what, int delayMillis) {
        Message msg = Message.obtain();
        msg.what = what;
        protocolHandler.sendMessageDelayed(msg, delayMillis);
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }
}
