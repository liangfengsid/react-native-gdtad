package cn.cnlee.commons.gdt.view;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.qq.e.ads.cfg.VideoOption;
import com.squareup.picasso.Picasso;
import com.vivo.mobilead.nativead.NativeAdParams;
import com.vivo.mobilead.nativead.VivoNativeAd;
import com.vivo.mobilead.unified.base.callback.MediaListener;
import com.vivo.mobilead.unified.base.view.NativeVideoView;
import com.vivo.ad.nativead.NativeAdListener;
import com.vivo.ad.nativead.NativeResponse;
import com.vivo.mobilead.unified.base.view.VivoNativeAdContainer;

import cn.cnlee.commons.gdt.R;
import cn.cnlee.commons.gdt.util.vivoad.VAdType;


public class VivoNativeExpress extends FrameLayout {

    private Context mContext;
    private VivoNativeAd mVivoNativeAd;
    private NativeVideoView mVideoView;
    private Runnable mLayoutRunnable;
    private boolean isRegister = true;
    private boolean isAutoPlay = false;

    public VivoNativeExpress(Context context, String posID, NativeAdListener listener) {
        this(context, null, posID, listener);
    }

    public VivoNativeExpress(Context context, AttributeSet attrs, String posID, NativeAdListener listener) {
        this(context, attrs, 0, posID, listener);
    }

    public VivoNativeExpress(Context context, AttributeSet attrs, int defStyleAttr, String posID, NativeAdListener listener) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        // 把布局加载到这个View里面
        inflate(context, R.layout.layout_banner,this);
        initView(posID, listener);
    }

    /**
     * 初始化View
     */
    private void initView(String posID, NativeAdListener listener) {
        closeNative();
        NativeAdParams.Builder builder = new NativeAdParams.Builder(posID);
        mVivoNativeAd = new VivoNativeAd((Activity) mContext, builder.build(), listener);
        mVivoNativeAd.loadAd();
        this.isAutoPlay = getVideoAutoPlay(mContext);
    }

    public void closeNative() {
        removeAllViews();
        if (mVideoView != null) {
            mVideoView.release();
        }
        if (mLayoutRunnable != null){
            removeCallbacks(mLayoutRunnable);
        }
    }

    /**
     * 无图片信息流广告展示, 无图样式展示广告标题、广告描述、广告ICON、广告标记信息
     */
    public void showNoneImageAd(NativeResponse response) {
        VivoNativeAdContainer adView =  (VivoNativeAdContainer) LayoutInflater.from(mContext).inflate(R.layout.layout_stream_no_image, null);
        ImageView ivIcon = adView.findViewById(R.id.iv_icon);
        TextView tvTitle = adView.findViewById(R.id.tv_title);
        TextView tvDesc = adView.findViewById(R.id.tv_desc);

        //设置标题、描述、ICON信息
        tvTitle.setText(response.getTitle());
        tvDesc.setText(response.getDesc());
        Picasso.with(mContext).load(response.getIconUrl()).into(ivIcon);

        //必须添加广告logo 否者审核不通过
        renderAdLogoAndTag(adView, response);
        //添加广告到视图树中
        this.addView(adView);
        response.registerView(adView, null,null);
    }

    /**
     * 小图信息流广告展示, 小图样式展示广告标题、广告ICON、广告标记信息
     */
    public void showTinyImageAd(NativeResponse response) {
        VivoNativeAdContainer adView = (VivoNativeAdContainer) LayoutInflater.from(mContext).inflate(R.layout.layout_stream_tiny_image, null);
        ImageView ivImage = adView.findViewById(R.id.iv_image);
        TextView tvTitle = adView.findViewById(R.id.tv_title);
        //设置广告图片
        Picasso.with(mContext).load(response.getImgUrl().get(0)).into(ivImage);
        //设置标题
        tvTitle.setText(response.getTitle());
        //必须添加广告logo 否者审核不通过
        renderAdLogoAndTag(adView, response);
        //添加广告到视图树中
        this.addView(adView);
        response.registerView(adView, null, null);
    }

    /**
     * 展示视频
     */
    public void showVideo(NativeResponse response, MediaListener listener) {
        VivoNativeAdContainer adView = (VivoNativeAdContainer) LayoutInflater.from(mContext).inflate(R.layout.layout_stream_video, null);
        mVideoView = adView.findViewById(R.id.nvv_video);
        Button btnInstall = adView.findViewById(R.id.btn_install);
        TextView tvTitle = adView.findViewById(R.id.tv_title);
        tvTitle.setText(response.getTitle());
        //必须添加广告logo 否者审核不通过
        renderAdLogoAndTag(adView, response);
        //添加广告到视图树中
        this.addView(adView);
        /**
         * 原生视频一定要注册这个方法，不然视频无法播放
         */
        response.registerView(adView, null, btnInstall, mVideoView);
        /**
         * 播放时机接入方可自己参照时机设置，但是一定要在registerView方法之后才能播放
         */
        if (this.isAutoPlay) {
            mVideoView.start();
        }
        mVideoView.setMediaListener(listener);
    }

    /**
     * 大图信息流广告展示, 大图样式根据广告类型不同展示内容会有所差异
     * <p>
     * 应用类广告需要展示 广告标题、广告ICON、广告大图、操作按钮、广告标记信息
     * 网址类广告需要展示 广告标题、广告大图、广告标记信息
     */
    public void showLargeImageAd(final NativeResponse response) {
        final VivoNativeAdContainer adView = (VivoNativeAdContainer) LayoutInflater.from(mContext).inflate(R.layout.layout_stream_large_image, null);
        final ImageView ivImage = adView.findViewById(R.id.iv_image);
        ImageView ivIcon = adView.findViewById(R.id.iv_icon);
        LinearLayout llAppInfo = adView.findViewById(R.id.ll_app_info);
        TextView tvAppTitle = adView.findViewById(R.id.tv_app_title);
        Button btnInstall = adView.findViewById(R.id.btn_install);
        TextView tvTitle = adView.findViewById(R.id.tv_title);

        //广告视图成功添加后，根据下发图片尺寸等比缩放 广告ImageView
        adView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                adView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            } else {
                adView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
            }

            int bitmapWidth = response.getImgDimensions()[0];
            int bitmapHeight = response.getImgDimensions()[1];
            float imageViewWidth = ivImage.getMeasuredWidth();
            int imageViewHeight = Math.round(imageViewWidth / bitmapWidth * bitmapHeight);

            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) ivImage.getLayoutParams();
            lp.height = imageViewHeight;
            ivImage.setLayoutParams(lp);

            Picasso.with(mContext).load(response.getImgUrl().get(0)).noFade().into(ivImage);
            }
        });
        //展示广告ICON
        if (!TextUtils.isEmpty(response.getIconUrl())) {
            Picasso.with(mContext).load(response.getIconUrl()).into(ivIcon);
        }
        //网址类广告不需要有应用信息
        if (response.getAdType() == VAdType.AD_WEBSITE) {
            llAppInfo.setVisibility(View.GONE);
            tvTitle.setText(response.getTitle());
        } else {
            tvTitle.setVisibility(View.GONE);
            tvAppTitle.setText(response.getTitle());
            //快生态不需要展示操作按钮
            if (response.getAdType() == VAdType.AD_RPK) {
                btnInstall.setVisibility(View.GONE);
            } else {
                setButton(btnInstall, response);
            }
        }
        //必须添加广告logo 否者审核不通过
        renderAdLogoAndTag(adView, response);
        this.addView(adView);
        if (isRegister) {
            response.registerView(adView, null, btnInstall);
        } else {
            response.registerView(adView, null,null);
        }
    }

    /**
     * 组图信息流广告展示, 组图样式根据广告类型不同展示内容会有所差异
     * <p>
     * 应用类广告需要展示 广告标题、广告组图、操作按钮、广告标记信息
     * 网址类广告需要展示 广告组图、广告标记信息
     */
    public void showMultiImageAd(final NativeResponse response) {
        final VivoNativeAdContainer adView = (VivoNativeAdContainer)LayoutInflater.from(mContext).inflate(R.layout.layout_stream_multi_image, null);
        final LinearLayout llMultiImage = adView.findViewById(R.id.ll_multi_image);
        final ImageView ivImage = adView.findViewById(R.id.iv_image);
        final ImageView ivImage1 = adView.findViewById(R.id.iv_image1);
        final ImageView ivImage2 = adView.findViewById(R.id.iv_image2);
        LinearLayout llAppInfo = adView.findViewById(R.id.ll_app_info);
        TextView tvAppTitle = adView.findViewById(R.id.tv_app_title);
        Button btnInstall = adView.findViewById(R.id.btn_install);

        //广告视图成功添加后，根据下发图片尺寸等比缩放 广告ImageView
        adView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                adView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            } else {
                adView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
            }

            int bitmapWidth = response.getImgDimensions()[0];
            int bitmapHeight = response.getImgDimensions()[1];
            int layoutWidth = llMultiImage.getMeasuredWidth();

            //组图样式 图片固定为 3 张, 根据图片规格等比缩放广告ImageView
            int adImageWidth = (layoutWidth - dp2px(mContext, 2)) / 3;
            int adImageHeight = Math.round(adImageWidth / bitmapWidth * bitmapHeight);

            LinearLayout.LayoutParams adLayoutParams = (LinearLayout.LayoutParams) ivImage.getLayoutParams();
            adLayoutParams.width = adImageWidth;
            adLayoutParams.height = adImageHeight;
            ivImage.setLayoutParams(adLayoutParams);

            LinearLayout.LayoutParams adLayoutParams1 = (LinearLayout.LayoutParams) ivImage1.getLayoutParams();
            adLayoutParams1.width = adImageWidth;
            adLayoutParams1.height = adImageHeight;
            ivImage1.setLayoutParams(adLayoutParams1);

            LinearLayout.LayoutParams adLayoutParams2 = (LinearLayout.LayoutParams) ivImage2.getLayoutParams();
            adLayoutParams2.width = adImageWidth;
            adLayoutParams2.height = adImageHeight;
            ivImage2.setLayoutParams(adLayoutParams2);

            Picasso.with(mContext).load(response.getImgUrl().get(0)).noFade().into(ivImage);
            Picasso.with(mContext).load(response.getImgUrl().get(1)).noFade().into(ivImage1);
            Picasso.with(mContext).load(response.getImgUrl().get(2)).noFade().into(ivImage2);
            }
        });

        //网址类广告不需要有应用信息
        if (response.getAdType() == VAdType.AD_WEBSITE) {
            llAppInfo.setVisibility(View.GONE);
        } else {
            tvAppTitle.setText(response.getTitle());
            //快生态不需要展示操作按钮
            if (response.getAdType() == VAdType.AD_RPK) {
                btnInstall.setVisibility(View.GONE);
            } else {
                setButton(btnInstall, response);
            }
        }
        //必须添加广告logo 否者审核不通过
        renderAdLogoAndTag(adView, response);
        //添加广告到视图树中
        this.addView(adView);
        if (isRegister) {
            response.registerView(adView, null, btnInstall);
        } else {
            response.registerView(adView, null,null);
        }
    }

    public static int dp2px(Context context, float dpVal) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                dpVal, context.getResources().getDisplayMetrics());
    }

    /**
     * 设置 大图样式、组图样式 广告 通用信息
     *
     * @param btn_adInstall
     */
    private void setButton(Button btn_adInstall, NativeResponse response) {
        //0：未安装 1：已安装
        switch (response.getAPPStatus()) {
            case 0:
                btn_adInstall.setBackgroundDrawable(getResources().getDrawable(R.drawable.bg_install_btn));
                break;
            case 1:
                btn_adInstall.setBackgroundDrawable(getResources().getDrawable(R.drawable.bg_open_btn));
                break;
            default:
                btn_adInstall.setBackgroundDrawable(getResources().getDrawable(R.drawable.bg_detail_btn));
                break;
        }
    }

    /**
     * 广告标记
     *
     * @param adView
     */
    private void renderAdLogoAndTag(View adView, NativeResponse response) {
        ImageView ivAdMarkLogo = adView.findViewById(R.id.iv_ad_mark_logo);
        TextView tvAdMarkText = adView.findViewById(R.id.tv_ad_mark_text);

        if (response.getAdLogo() != null) {
            ivAdMarkLogo.setVisibility(View.VISIBLE);
            tvAdMarkText.setVisibility(View.GONE);
            ivAdMarkLogo.setImageBitmap(response.getAdLogo());
        } else if (!TextUtils.isEmpty(response.getAdMarkUrl())) {
            ivAdMarkLogo.setVisibility(View.VISIBLE);
            tvAdMarkText.setVisibility(View.GONE);
            Picasso.with(mContext)
                    .load(response.getAdMarkUrl())
                    .into(ivAdMarkLogo);
        } else {
            String adMark;
            if (!TextUtils.isEmpty(response.getAdMarkText())) {
                adMark = response.getAdMarkText();
            } else if (!TextUtils.isEmpty(response.getAdTag())) {
                adMark = response.getAdTag();
            } else {
                adMark = "广告";
            }

            tvAdMarkText.setVisibility(View.VISIBLE);
            ivAdMarkLogo.setVisibility(View.GONE);
            tvAdMarkText.setText(adMark);
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        closeNative();
        super.onDetachedFromWindow();
    }

    @Override
    public void requestLayout() {
        super.requestLayout();
        if (mLayoutRunnable != null){
            removeCallbacks(mLayoutRunnable);
        }
        mLayoutRunnable = new Runnable() {
            @Override
            public void run() {
                measure(
                        MeasureSpec.makeMeasureSpec(getWidth(), MeasureSpec.EXACTLY),
                        MeasureSpec.makeMeasureSpec(getHeight(), MeasureSpec.EXACTLY));
                layout(getLeft(), getTop(), getRight(), getBottom());
            }
        };
        post(mLayoutRunnable);
    }

    public static boolean getVideoAutoPlay(Context context){
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifiNetworkInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        return wifiNetworkInfo != null && wifiNetworkInfo.isConnected() ? true:false;
    }
}
