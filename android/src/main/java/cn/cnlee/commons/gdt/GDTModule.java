package cn.cnlee.commons.gdt;

import com.facebook.react.bridge.ActivityEventListener;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.bridge.Callback;

import com.huawei.agconnect.remoteconfig.AGConnectConfig;
import com.huawei.agconnect.remoteconfig.ConfigValues;
import com.huawei.hmf.tasks.OnFailureListener;
import com.huawei.hmf.tasks.OnSuccessListener;
import com.huawei.hms.analytics.HiAnalytics;
import com.huawei.hms.analytics.HiAnalyticsInstance;
import com.huawei.hms.analytics.HiAnalyticsTools;
import com.huawei.hms.jos.AppParams;
import com.huawei.hms.jos.JosAppsClient;
import com.huawei.hms.support.account.request.AccountAuthParams;
import com.qq.e.comm.managers.GDTAdSdk;
import com.qq.e.comm.managers.setting.GlobalSetting;
import cn.cnlee.commons.gdt.view.Hybrid;
import cn.cnlee.commons.gdt.view.RewardVideo;

import com.huawei.hms.jos.AppUpdateClient;
import com.huawei.hms.jos.JosApps;
import com.huawei.updatesdk.service.appmgr.bean.ApkUpgradeInfo;
import com.huawei.updatesdk.service.otaupdate.CheckUpdateCallBack;
import com.huawei.updatesdk.service.otaupdate.UpdateKey;

import com.huawei.openalliance.ad.inter.HiAd;
import com.huawei.hms.ads.HwAds;

// Vivo account sdk
import com.bbk.account.oauth.constant.Constant;
import com.bbk.account.oauth.Oauth;
import com.bbk.account.oauth.OauthCallback;
import com.bbk.account.oauth.OauthResult;
// Vivo payment sdk
import com.vivo.mobilead.manager.VivoAdManager;
import com.vivo.mobilead.util.VOpenLog;
import com.vivo.unionsdk.open.VivoConfigInfo;
import com.vivo.unionsdk.open.VivoUnionSDK;
import com.vivo.unionsdk.open.OrderResultInfo;
import com.vivo.unionsdk.open.VivoConstants;
import com.vivo.unionsdk.open.VivoPayCallback;
import com.vivo.unionsdk.open.VivoPayInfo;
import cn.cnlee.commons.gdt.util.VivoPayInfoUtils;
import cn.cnlee.commons.gdt.view.VivoRewardVideo;
import cn.cnlee.commons.gdt.view.VivoSplashActivity;

// For facebook sdk
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;

import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import java.io.Serializable;
import java.util.Date;

public class GDTModule extends ReactContextBaseJavaModule implements ActivityEventListener {

    private static final String TAG = "GDTModule";
    private static final int LAUNCH_VIVO_SPLASH = 10001;
    private ReactApplicationContext mContext;
    private Callback mOnVivoSplashResult;

    public GDTModule(ReactApplicationContext reactContext) {
        super(reactContext);
        reactContext.addActivityEventListener(this);
        mContext = reactContext;
    }

    @Override
    public String getName() {
        return TAG;
    }

    @ReactMethod
    public void init(String appID) {
        // Init GDT sdk
        GDTAdSdk.init(mContext, appID);
    }

    @ReactMethod
    public void setGDTPersonalizedOff(int off) {
        GlobalSetting.setPersonalizedState(off);
    }

    @ReactMethod
    public void initHMS() {
        // hms lianyun app init
        AccountAuthParams params = AccountAuthParams.DEFAULT_AUTH_REQUEST_PARAM;
        JosAppsClient appsClient = JosApps.getJosAppsClient(getCurrentActivity());
        appsClient.init(new AppParams(params));

        // Hw hms
        HiAnalyticsTools.enableLog();
        HiAnalyticsInstance instance = HiAnalytics.getInstance(mContext);
        AGConnectConfig config = AGConnectConfig.getInstance();
        Bundle bundle = new Bundle();
        String launchTime = new Date().toString();
        bundle.putString("start app",launchTime);
        instance.onEvent("launch app",bundle);
        config.fetch().addOnSuccessListener(new OnSuccessListener<ConfigValues>() {
            @Override
            public void onSuccess(ConfigValues configValues) {
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(Exception e) {
            }
        });
    }

    @ReactMethod
    public void initHMSAd() {
        // Init HMS ad sdk
        HwAds.init(mContext);
        HiAd.getInstance(mContext).enableUserInfo(true);
        HiAd.getInstance(mContext).initLog(true, Log.INFO);
    }

    @ReactMethod
    public void initVivoSDK(final String appID, final boolean debug) {
        // Init Vivo sdk
        //初始化，并配置不展示游戏悬浮球
        final VivoConfigInfo configInfo = new VivoConfigInfo();
        //不显示悬浮球，若需要展示，则去掉此行代码即可
        configInfo.setShowAssit(false);
        new Handler(mContext.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                VivoUnionSDK.initSdk(mContext, appID, debug, configInfo);
            }
        });
    }

    /**
     * 调起vivo收银台支付
     */
    @ReactMethod
    public void vivoPayNormal(String appID, String appKey, final String orderNumber, String extInfo, String notifyUrl, String orderAmount, String productName, String productDesc, final Callback onResult) {
        VivoPayCallback mVivoPayCallback = new VivoPayCallback() {
            // 客户端返回的支付结果不可靠，请再查询服务器，以服务器端最终的支付结果为准；
            @Override
            public void onVivoPayResult(int code, OrderResultInfo orderResultInfo) {
                Log.i(TAG, "onVivoPayResult, result= " + code + "vivoOrderNumber=" + orderResultInfo.getTransNo());
                String result;
                if (code == VivoConstants.PAYMENT_RESULT_CODE_SUCCESS) {
                    result = "支付成功";
                } else if (code == VivoConstants.PAYMENT_RESULT_CODE_CANCEL) {
                    result = "取消支付";
                } else if (code == VivoConstants.PAYMENT_RESULT_CODE_UNKNOWN) {
                    result = "未知状态，请查询服务器订单状态";
                } else {
                    result = "支付失败";
                }
                WritableMap resultMap = Arguments.createMap();
                resultMap.putInt("errCode", code);
                resultMap.putString("msg", result);
                onResult.invoke(resultMap);
            }

        };
        VivoPayInfo vivoPayInfo = VivoPayInfoUtils.createPayInfo(appID, appKey, orderNumber, extInfo, notifyUrl, orderAmount, productName, productDesc);
        VivoUnionSDK.payV2(getCurrentActivity(), vivoPayInfo, mVivoPayCallback);
    }

    /**
     * 调起vivo登录
     */
    @ReactMethod
    public void vivoLogin(String appID, String redirectUri, final Callback onResult) {
//        Oauth.Builder builder = new Oauth.Builder(getCurrentActivity());
        Oauth auth=new Oauth.Builder(getCurrentActivity()).setAppID(appID).setRedirectUrl(redirectUri).setSilentAuth(false).setUseSDKPermissionActivity(true).setOauthStyle(Constant.OauthStyle.STYEL_DIALOG).setKeepCookie(false).build();
        auth.requestCode(new OauthCallback() {
            @Override
            public void onStartLoading() {
                // 授权开始，应用可在此处显示loading圈
            }
            @Override
            public void onResult(OauthResult result) {
                if (onResult!=null) {
                    WritableMap resultMap = Arguments.createMap();
                    resultMap.putInt("errCode", result.getStatusCode());
                    resultMap.putString("code", result.getCode());
                    onResult.invoke(resultMap);
                }
            }
            @Override
            public void onEndLoading() {
                // 授权结束，应用可在此处关闭loading圈
            }
        });

    }

    @ReactMethod
    public void initVivoAdSDK(final String mediaID) {
        // Init Vivo ad sdk
        new Handler(mContext.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                VivoAdManager.getInstance().init((Application) mContext.getApplicationContext(), mediaID);
                VOpenLog.setEnableLog(true);
            }
        });

        // 开启热启动开屏功能，SDK 默认关闭此功能需要手动开启才能生效
        // 广告位 id 可以共用冷启动开屏广告位 id，也可单独为热启动再申请一个新广告位 id // 方向参数只能是 SplashAdParams.ORIENTATION_LANDSCAPE
        // 或 SplashAdParams.ORIENTATION_PORTRAIT 之一
        // 热启动开屏可以和冷启动开屏方向不一致，但是强烈建议两者保持一致
//        VivoAdManager.getInstance().enableHotSplash((Application) mContext.getApplicationContext(), splashAdID, 1);
    }

    @ReactMethod
    public void checkHmsJosUpdate(final Callback onUpdate) {
        class UpdateCallBack implements CheckUpdateCallBack {
            private Callback callback;
            private UpdateCallBack(Callback callback) {
                this.callback = callback;
            }
            public void onUpdateInfo(Intent intent) {
                if (intent != null) {
                    // 获取更新状态码， Default_value为取不到status时默认的返回码，由应用自行决定
                    int status = intent.getIntExtra(UpdateKey.STATUS, 0);
                    // 错误码，建议打印
                    int rtnCode = intent.getIntExtra(UpdateKey.FAIL_CODE, 0);
                    // 失败信息，建议打印
                    String rtnMessage = intent.getStringExtra(UpdateKey.FAIL_REASON);
                    Serializable info = intent.getSerializableExtra(UpdateKey.INFO);
                    //可通过获取到的info是否属于ApkUpgradeInfo类型来判断应用是否有更新
                    if (info instanceof ApkUpgradeInfo) {
                        String version = ((ApkUpgradeInfo) info).getVersion_();
                        String url = ((ApkUpgradeInfo) info).getDownurl_();
                        String features = ((ApkUpgradeInfo) info).getNewFeatures_();
                        String infoStr = ((ApkUpgradeInfo) info).toJson();
                        final WritableMap infoMap = Arguments.createMap();
                        infoMap.putString("version", version);
                        infoMap.putString("url", url);
                        infoMap.putString("features", features);
                        infoMap.putString("json", infoStr);
                        this.callback.invoke(infoMap);
                        return;
                    }
                }
                this.callback.invoke("");
            }

            @Override
            public void onMarketInstallInfo(Intent intent) {
            }

            @Override
            public void onMarketStoreError(int i) {
            }

            @Override
            public void onUpdateStoreError(int i) {
            }
        }

        AppUpdateClient client = JosApps.getAppUpdateClient(mContext);
        client.checkAppUpdate(mContext, new UpdateCallBack(onUpdate));
    }

    @ReactMethod
    public void showRewardVideoAD(String posID, Callback onReward) {
        RewardVideo rewardVideo = RewardVideo.getInstance(mContext, onReward);
        rewardVideo.showRewardVideoAD(posID);
    }

    @ReactMethod
    public void showVivoRewardVideoAD(String posID, Callback onReward) {
        VivoRewardVideo rewardVideo = VivoRewardVideo.getInstance(getCurrentActivity(), onReward);
        rewardVideo.showRewardVideoAD(posID);
    }

    @ReactMethod
    public void showVivoSplashAD(final String appTitle, final String appDesc, final String posID, final Callback onResult) {
//        new Handler(mContext.getMainLooper()).post(new Runnable() {
//            @Override
//            public void run() {
//
//            }
//        });
        mOnVivoSplashResult = onResult;
        Intent intent = new Intent(mContext, VivoSplashActivity.class);
        intent.putExtra("posID", posID);
        intent.putExtra("appTitle", appTitle);
        intent.putExtra("appDesc", appDesc);
        mContext.startActivityForResult(intent, LAUNCH_VIVO_SPLASH, null);
//        VivoSplash splash = VivoSplash.getInstance(mContext.getCurrentActivity(), onResult);
//        splash.showVivoSplashAD(appTitle, appDesc, posID);
    }

    /***
     *
     * @param appID
     * @param url
     * @param settings ["titleBarHeight", "titleBarColor", "title", "titleColor", "titleSize", "backButtonImage", "closeButtonImage", "separatorColor", "backSeparatorLength"]
     * titleColor='#ff0000ff',titleBarHeight=45 dp, titleSize=20 sp, backButtonImage='gdt_ic_back'
     */
    @ReactMethod
    public void openWeb(String appID, String url, ReadableMap settings) {
        Hybrid hybrid = Hybrid.getInstance(mContext);
        hybrid.openWeb(appID, url, settings);
    }

    @Override
    public void onActivityResult(Activity activity, int requestCode, int resultCode, Intent data) {
        Log.i(TAG, "onActivityResult, requestCode: " + requestCode + ", resultCode:" + resultCode);
        if (requestCode == LAUNCH_VIVO_SPLASH) {
            if(resultCode == Activity.RESULT_OK){
                String result = data.getStringExtra("result");
                String msg = data.getStringExtra("msg");
                final WritableMap event = Arguments.createMap();
                event.putString("result", result);
                event.putString("msg", msg);
                Log.i(TAG, "onActivityResult, request: " + result + ", msg: " + msg);
                if (mOnVivoSplashResult != null) {
                    Log.i(TAG, "to invoke mOnVivoSplashResult, result: " + result + ", msg: " + msg);
                    mOnVivoSplashResult.invoke(event);
                }
            } else {
                Log.i(TAG, "onActivityResult null");
                if (mOnVivoSplashResult != null) {
                    mOnVivoSplashResult.invoke(null);
                }
            }
        }
    }

    @ReactMethod
    public void initFacebookSdk() {
        // Facebook sdk
        FacebookSdk.sdkInitialize(mContext);
        AppEventsLogger.activateApp(getCurrentActivity());
    }

    @Override
    public void onNewIntent(Intent intent) {

    }
}
