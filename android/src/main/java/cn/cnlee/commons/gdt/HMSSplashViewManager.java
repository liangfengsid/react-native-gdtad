package cn.cnlee.commons.gdt;

import android.app.Activity;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.common.MapBuilder;
import com.facebook.react.uimanager.SimpleViewManager;
import com.facebook.react.uimanager.ThemedReactContext;
import com.facebook.react.uimanager.annotations.ReactProp;
import com.facebook.react.uimanager.events.RCTEventEmitter;
import com.google.gson.Gson;

import com.huawei.hms.ads.splash.SplashAdDisplayListener;
import com.huawei.hms.ads.splash.SplashView;

import java.util.Map;

import javax.annotation.Nullable;

import cn.cnlee.commons.gdt.view.HMSSplash;

public class HMSSplashViewManager extends SimpleViewManager {

    private static final String TAG = "HMSSplash";

    // 重写getName()方法, 返回的字符串就是RN中使用该组件的名称
    @Override
    public String getName() {
        return TAG;
    }

    // SplashView.SplashAdLoadListener interfaces
    private SplashView.SplashAdLoadListener splashAdLoadListener = new SplashView.SplashAdLoadListener() {
        /**
         *  获取广告失败
         * @param errorCode 错误码
         */
        @Override
        public void onAdFailedToLoad(int errorCode) {
            Log.i(TAG, "onNoAD: eCode=" + errorCode);
            final WritableMap event = Arguments.createMap();
            event.putString("error", ""+errorCode);
            mEventEmitter.receiveEvent(mContainer.getId(), Events.EVENT_FAIL_TO_RECEIVED.toString(), event);
        }

        /**
         * 获取广告成功
         */
        @Override
        public void onAdLoaded() {
            Log.d(TAG, "onAdLoaded");
//            if (mSplash != null) {
//                mSplash.invalidate();
//                mSplash.requestLayout();
//            }
            mEventEmitter.receiveEvent(mContainer.getId(), Events.EVENT_LOADED.toString(), null);
        }

        /**
         * 广告或Slogan展示结束
         */
        @Override
        public void onAdDismissed() {
            Log.d(TAG, "onAdDismissed");
            mEventEmitter.receiveEvent(mContainer.getId(), Events.EVENT_DISMISSED.toString(), null);
        }
    };

    // SplashAdDisplayListener interfaces
    private SplashAdDisplayListener adDisplayListener = new SplashAdDisplayListener() {
        @Override
        public void onAdShowed() {
            // Call this method when an ad is displayed.
            Log.d(TAG, "SplashAdDisplayListener onAdShowed.");
            if (mSplash != null) {
                mSplash.invalidate();
                mSplash.requestLayout();
            }
            mEventEmitter.receiveEvent(mContainer.getId(), Events.EVENT_EXPOSURED.toString(), null);
        }

        @Override
        public void onAdClick() {
            // Call this method when an ad is clicked.
            Log.d(TAG, "SplashAdDisplayListener onAdClick.");
            mEventEmitter.receiveEvent(mContainer.getId(), Events.EVENT_ON_CLICK.toString(), null);
        }
    };
    
    public enum Events {
        EVENT_FAIL_TO_RECEIVED("onFailToReceived"),
        EVENT_LOADED("onLoaded"),
        EVENT_DISMISSED("onDismissed"),
        EVENT_EXPOSURED("onExposured"),
        EVENT_ON_CLICK("onClicked");

        private final String mName;

        Events(final String name) {
            mName = name;
        }

        @Override
        public String toString() {
            return mName;
        }
    }

    private FrameLayout mContainer;
    private RCTEventEmitter mEventEmitter;
    private ThemedReactContext mThemedReactContext;
    private HMSSplash mSplash;

    // private Handler handler = new Handler(Looper.getMainLooper());


    @Override
    protected View createViewInstance(ThemedReactContext reactContext) {
        mThemedReactContext = reactContext;
        mEventEmitter = reactContext.getJSModule(RCTEventEmitter.class);
        FrameLayout viewGroup = new FrameLayout(reactContext);
        mContainer = viewGroup;
        return viewGroup;
    }

    @Nullable
    @Override
    public Map<String, Object> getExportedCustomDirectEventTypeConstants() {
        MapBuilder.Builder<String, Object> builder = MapBuilder.builder();
        for (Events event : Events.values()) {
            builder.put(event.toString(), MapBuilder.of("registrationName", event.toString()));
        }
        return builder.build();
    }

    // 其中，可以通过@ReactProp（或@ReactPropGroup）注解来导出属性的设置方法。
    // 该方法有两个参数，第一个参数是泛型View的实例对象，第二个参数是要设置的属性值。
    // 方法的返回值类型必须为void，而且访问控制必须被声明为public。
    // 组件的每一个属性的设置都会调用Java层被对应ReactProp注解的方法
    @ReactProp(name = "appInfo")
    public void setAppInfo(FrameLayout view, ReadableMap appInfo) {
        String posID = appInfo.getString("posId");
        if (posID != null && posID != "") {
            if (mThemedReactContext.hasCurrentActivity()) {
                HMSSplash splash = new HMSSplash(mThemedReactContext.getCurrentActivity(), posID, this.splashAdLoadListener, this.adDisplayListener);
                mSplash = splash;
                view.addView(splash);
            }
        } 
    }

}
