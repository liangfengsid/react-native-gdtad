package cn.cnlee.commons.gdt.view;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.WindowManager;

import com.facebook.react.bridge.Callback;
import com.vivo.ad.video.VideoAdListener;
import com.vivo.mobilead.video.VideoAdParams;
import com.vivo.mobilead.video.VivoVideoAd;

/**
 *
 *  视频资源是异步加载的，但是加载回调都会在主线程中回调
 *
 *  视频播放只能在主线程中执行，异步播放会出错。
 *  由于是在主线程中播放，所以播放过程中的所有 播放回调 都是在主线程中回调的。
 */

public class VivoRewardVideo implements VideoAdListener {

    private static final String TAG = VivoRewardVideo.class.getSimpleName();

    private static VivoRewardVideo mInstance;
    private VivoVideoAd mVivoVideoAd;
    private String posID;
    private Callback mOnReward;

    private Context mContext;

    public static VivoRewardVideo getInstance(Context context, Callback onReward) {
        if (mInstance == null) {
            mInstance = new VivoRewardVideo(context, onReward);
        }
        mInstance.mOnReward = onReward;
        return mInstance;
    }

    private VivoRewardVideo(Context context, Callback onReward){
        this.mContext = context;
        this.mOnReward = onReward;
        ((Activity)context).getWindow().setFlags(WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED, WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED);
    }

    public void showRewardVideoAD(String posID) {
        Log.i(TAG, "showRewardVideoAD");
        getVideoAD(posID).loadAd();
    }

    private VivoVideoAd getVideoAD(String posID) {
        if (mVivoVideoAd != null && this.posID!= null &&this.posID.equals(posID)) {
            Log.i(TAG,"======相同IAD无需创建新的======");
            return mVivoVideoAd;
        }
        VideoAdParams.Builder builder = new VideoAdParams.Builder(posID);
        mVivoVideoAd = new VivoVideoAd((Activity) mContext, builder.build(), this);
        return mVivoVideoAd;
    }

    @Override
    public void onAdLoad() {
        Log.i(TAG,"onADLoad");
        mVivoVideoAd.showAd((Activity) mContext);
    }

    @Override
    public void onAdFailed(String s) {
        Log.i(TAG, "onAdFailed");
        if (this.mOnReward != null) {
            this.mOnReward.invoke("Ad failed: "+s);
        }
    }

    @Override
    public void onVideoStart() {
        Log.i(TAG, "onVideoStart");
    }

    @Override
    public void onVideoCompletion() {
        Log.i(TAG, "onVideoCompletion");
    }

    @Override
    public void onVideoClose(int i) {
        Log.i(TAG, "onVideoClose");
    }

    @Override
    public void onVideoCloseAfterComplete() {
        Log.i(TAG, "onVideoCloseAfterComplete");
        if (this.mOnReward != null) {
            this.mOnReward.invoke("");
        }
    }

    @Override
    public void onVideoError(String s) {
        Log.i(TAG, "onVideoError");
        if (this.mOnReward != null) {
            this.mOnReward.invoke("Ad video failed: "+s);
        }
    }

    @Override
    public void onFrequency() {
        Log.i(TAG, "onFrequency");
    }

    @Override
    public void onNetError(String s) {
        Log.i(TAG, "onNetError");
        if (this.mOnReward != null) {
            this.mOnReward.invoke("Ad network error: "+s);
        }
    }

    @Override
    public void onRequestLimit() {
        Log.i(TAG, "onRequestLimit");
    }
}
