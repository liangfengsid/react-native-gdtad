package cn.cnlee.commons.gdt.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.KeyEvent;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.WritableMap;
import com.google.gson.Gson;
import com.vivo.ad.model.AdError;
import com.vivo.ad.splash.SplashAdListener;
import com.vivo.mobilead.model.BackUrlInfo;
import com.vivo.mobilead.splash.SplashAdParams;
import com.vivo.mobilead.splash.VivoSplashAd;

import cn.cnlee.commons.gdt.R;

public class VivoSplashActivity extends Activity implements SplashAdListener {
    private static final String TAG = "VivoSplashActivity";

    private int minSplashTimeWhenNoAD = 2000;
    private long fetchSplashADTime;
    private VivoSplashAd mVivoSplashAd;
    public boolean canJump = false;

    private String mPosID;
    private String mAppTitle;
    private String mAppDesc;

    private Handler handler = new Handler(Looper.getMainLooper());

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LOCKED);
        mPosID = getIntent().getStringExtra("posID");
        mAppTitle = getIntent().getStringExtra("appTitle");
        mAppDesc = getIntent().getStringExtra("appDesc");
        mVivoSplashAd = getSplashAD(mAppTitle, mAppDesc, mPosID, this);
        mVivoSplashAd.loadAd();
    }

    private VivoSplashAd getSplashAD(String appTitle, String appDesc, String posID, SplashAdListener listener) {
        fetchSplashADTime = System.currentTimeMillis();
        SplashAdParams.Builder builder = new SplashAdParams.Builder(posID);
        // 拉取广告的超时时长：即开屏广告从请求到展示所花的最大时长（并不是指广告曝光时长）取值范围[3000, 5000]
        builder.setFetchTimeout(3000);
        /**
         * 标题最长5个中文字符 描述最长8个中文字符
         */
        builder.setAppTitle(appTitle);
        /**
         * 广告下面半屏的应用标题+应用描述:应用标题和应用描述是必传字段，不传将抛出异常
         */
        builder.setAppDesc(appDesc);
        String backUrl = "";
        String btnName = "Paidu";
        builder.setBackUrlInfo(new BackUrlInfo(backUrl, btnName));
        builder.setSplashOrientation(SplashAdParams.ORIENTATION_PORTRAIT);
        builder.setSupportCustomView(true);
        builder.addCustomSplashBottomView(R.layout.layout_logo);

        mVivoSplashAd = new VivoSplashAd(this, listener, builder.build());
        return mVivoSplashAd;
    }

    @Override
    public void onADDismissed() {
        Log.i(TAG, "onDismissed");
        next("onDismissed", "");
    }

    @Override
    public void onNoAD(final AdError adError) {
        Log.i(TAG, "onNoAD: eCode=" + adError.getErrorCode() + ",eMsg=" + adError.getErrorMsg());
        long alreadyDelayMills = System.currentTimeMillis() - fetchSplashADTime;//从拉广告开始到onNoAD已经消耗了多少时间
        //为防止加载广告失败后立刻跳离开屏可能造成的视觉上类似于"闪退"的情况，根据设置的minSplashTimeWhenNoAD 计算出还需要延时多久
        long shouldDelayMills = alreadyDelayMills > minSplashTimeWhenNoAD ? 0 : minSplashTimeWhenNoAD - alreadyDelayMills;
        Log.i(TAG, "shouldDelayMills: " + shouldDelayMills);
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mVivoSplashAd != null) {
                    mVivoSplashAd.close();
                }
                toNextActivity("onFailToReceived", new Gson().toJson(adError));
            }
        }, shouldDelayMills);
    }

    @Override
    public void onADPresent() {
        Log.i(TAG, "onADPresent");
    }

    @Override
    public void onADClicked() {
        Log.i(TAG, "onADClicked");
    }

    /**
     * 设置一个变量来控制当前开屏页面是否可以跳转，当广告被点击，会跳转其他页面，此时开发者还不能打开自己的App主页。当从其他页面返回以后， 才可以跳转到开发者自己的App主页；
     */
    private void next(String result, String msg) {
        if (canJump) {
            toNextActivity(result, msg);
        } else {
            canJump = true;
        }
    }

    private void toNextActivity(String result, String msg) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra("result", result);
        returnIntent.putExtra("msg", msg);
        setResult(Activity.RESULT_OK,returnIntent);
        this.finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        canJump = false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (canJump) {
            next("onDismissed", "");
        }
        canJump = true;
    }

    /**
     * 开屏页一定要禁止用户对返回按钮的控制，
     * 否则将可能导致用户手动退出了App而广告无法正常曝光和计费
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK || keyCode == KeyEvent.KEYCODE_HOME) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
