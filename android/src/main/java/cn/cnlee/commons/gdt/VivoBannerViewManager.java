package cn.cnlee.commons.gdt;

import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.common.MapBuilder;
import com.facebook.react.uimanager.SimpleViewManager;
import com.facebook.react.uimanager.ThemedReactContext;
import com.facebook.react.uimanager.annotations.ReactProp;
import com.facebook.react.uimanager.events.RCTEventEmitter;
import com.google.gson.Gson;
import com.vivo.mobilead.listener.IAdListener;
import com.vivo.mobilead.model.VivoAdError;

import java.util.Map;

import javax.annotation.Nullable;

import cn.cnlee.commons.gdt.view.UnifiedBanner;
import cn.cnlee.commons.gdt.view.VivoBanner;

public class VivoBannerViewManager extends SimpleViewManager implements IAdListener {

    private static final String TAG = "VivoBanner";

    // 重写getName()方法, 返回的字符串就是RN中使用该组件的名称
    @Override
    public String getName() {
        return TAG;
    }

    @Override
    public void onAdShow() {
        Log.i(TAG,"onADExposure");
        mEventEmitter.receiveEvent(mContainer.getId(), Events.EVENT_WILL_EXPOSURE.toString(), null);
    }

    @Override
    public void onAdFailed(VivoAdError vivoAdError) {
        Log.i(TAG,"onNoAD: eCode=" + vivoAdError.getErrorCode() + ",eMsg=" + vivoAdError.getErrorMsg());
        WritableMap event = Arguments.createMap();
        event.putString("error", new Gson().toJson(vivoAdError));
        mEventEmitter.receiveEvent(mContainer.getId(), Events.EVENT_FAIL_TO_RECEIVED.toString(), event);
    }

    @Override
    public void onAdReady() {
        Log.i(TAG,"onADReceive");
        mEventEmitter.receiveEvent(mContainer.getId(), Events.EVENT_RECEIVED.toString(), null);
    }

    @Override
    public void onAdClick() {
        Log.i(TAG,"onADClicked");
        mEventEmitter.receiveEvent(mContainer.getId(), Events.EVENT_ON_CLICK.toString(), null);
    }

    @Override
    public void onAdClosed() {
        Log.i(TAG,"onADClosed");
        mEventEmitter.receiveEvent(mContainer.getId(), Events.EVENT_WILL_CLOSE.toString(), null);
    }

    public enum Events {
        EVENT_FAIL_TO_RECEIVED("onFailToReceived"),
        EVENT_RECEIVED("onReceived"),
        EVENT_WILL_CLOSE("onViewWillClose"),
        EVENT_WILL_EXPOSURE("onViewWillExposure"),
        EVENT_ON_CLICK("onClicked");

        private final String mName;

        Events(final String name) {
            mName = name;
        }

        @Override
        public String toString() {
            return mName;
        }
    }

    private FrameLayout mContainer;
    private RCTEventEmitter mEventEmitter;
    private ThemedReactContext mThemedReactContext;
    private VivoBanner mBanner;

    @Override
    protected View createViewInstance(ThemedReactContext reactContext) {
        mThemedReactContext = reactContext;
        mEventEmitter = reactContext.getJSModule(RCTEventEmitter.class);
        FrameLayout container = new FrameLayout(reactContext);
        mContainer = container;
        return container;
    }

    @Nullable
    @Override
    public Map<String, Object> getExportedCustomDirectEventTypeConstants() {
        MapBuilder.Builder<String, Object> builder = MapBuilder.builder();
        for (Events event : Events.values()) {
            builder.put(event.toString(), MapBuilder.of("registrationName", event.toString()));
        }
        return builder.build();
    }

    // 其中，可以通过@ReactProp（或@ReactPropGroup）注解来导出属性的设置方法。
    // 该方法有两个参数，第一个参数是泛型View的实例对象，第二个参数是要设置的属性值。
    // 方法的返回值类型必须为void，而且访问控制必须被声明为public。
    // 组件的每一个属性的设置都会调用Java层被对应ReactProp注解的方法
    @ReactProp(name = "appInfo")
    public void setAppInfo(FrameLayout view, final ReadableMap appInfo) {
        String posID = appInfo.getString("posId");
        VivoBanner banner = new VivoBanner(mThemedReactContext.getCurrentActivity(), posID, this);
        mBanner = banner;
        view.addView(banner);
    }

}
