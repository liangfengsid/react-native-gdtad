package cn.cnlee.commons.gdt.util.vivoad;

/**
 * @author 11101035
 */
public final class Constants {

    public interface ConfigureKey {
        String SERVER_URL = "server_url";
        String MEDIA_ID = "media_id";
        String SPLASH_POSITION_ID = "splash_position_id";
        String BANNER_POSITION_ID = "banner_position_id";
        String INTERSTITIAL_POSITION_ID = "interstitial_position_id";
        String NATIVE_POSITION_ID = "native_position_id";
        String NATIVE_STREAM_POSITION_ID = "native_stream_position_id";
        String VIDEO_POSITION_ID = "video_position_id";
        String SPLASH_AD_TIME = "splash_ad_time";
        String BANNER_AD_TIME = "banner_ad_time";
        String APP_TITLE = "app_title";
        String APP_DESC = "app_desc";
        String HOT_SPLASH = "hot_splash";
    }


    public interface DefaultConfigValue {
        /**
         * 测试用，接入方置空即可
         */
        String SERVER_URL = "http://10.101.19.148";
        /**
         * 以下ID需填入自己在广告后台申请的相关id
         */
//        String MEDIA_ID = "03ad601d82b64f8e9b4939259b47c26e";
//        String SPLASH_POSITION_ID = "7322e69685e44acca72d467c4537f847";
//        String BANNER_POSITION_ID = "88139a5007d84bdf88693c4a0f5460f0";
//        String INTERSTITIAL_POSITION_ID = "747f550e01eb4b87878264f293bfefe5";
//        String NATIVE_STREAM_POSITION_ID = "a45047f634d24753911ac976e4fa7ae8";
//        String VIDEO_POSITION_ID = "fbf56eb035f1481dafcc6bd62954ab3f";

        String MEDIA_ID = "b220712c388141d6a5cb944980c3f57b";
        String SPLASH_POSITION_ID = "ee6a278a7d64452caa4e571a69f08f0d";
        String BANNER_POSITION_ID = "646f81ed5f4f4e57a9d426905c88ffef";
        String INTERSTITIAL_POSITION_ID = "115d73772c2a46d8b56e4a4465b80134";
        String NATIVE_STREAM_POSITION_ID = "8ddd6670bfd541e9b48b1e3e104a50a4";
        String VIDEO_POSITION_ID = "a260ff8b2ee148a4910e9bf55d18489a";

        int SPLASH_AD_TIME = 3;
        int BANNER_AD_TIME = 15;
        String APP_TITLE = "开心消消乐";
        String APP_DESC = "娱乐休闲首选游戏";

        int HOT_SPLASH = 1; //-1: 关、1:竖屏、2:横屏
    }
}
