package cn.cnlee.commons.gdt.util;

import com.vivo.unionsdk.open.VivoPayInfo;

import java.util.HashMap;

/**
 * 订单信息生成工具类，用于生成相关支付参数，并计算支付参数验签
 */
public class VivoPayInfoUtils {

    //TODO 以下常量最终参与支付参数验签，请保证一致

    // appId
    public static final String APP_ID_PARAM = "appId";
    // cp订单号
    public static final String CP_ORDER_NUMBER = "cpOrderNumber";
    // 扩展参数
    public static final String EXT_INFO = "extInfo";
    // 回调地址
    public static final String NOTIFY_URL = "notifyUrl";
    // 商品金额
    public static final String ORDER_AMOUNT = "orderAmount";
    // 商品描述
    public static final String PRODUCT_DESC = "productDesc";
    // 商品名称
    public static final String PRODUCT_NAME = "productName";


    /**
     * 计算支付参数验签
     * TODO 为确保安全，有服务器的游戏，请让服务器去计算验签；如果没有服务器，可以通过这段代码去生成验签
     */
    public static String getSignature(String appID, String appKey, String orderNumber, String extInfo, String notifyUrl, String orderAmount, String productName, String productDesc) {
        HashMap<String, String> params = new HashMap<>();
        params.put(APP_ID_PARAM, appID);
        params.put(CP_ORDER_NUMBER, orderNumber);
        params.put(EXT_INFO, extInfo);
        params.put(NOTIFY_URL, notifyUrl);
        params.put(ORDER_AMOUNT, orderAmount);
        params.put(PRODUCT_NAME, productName);
        params.put(PRODUCT_DESC, productDesc);
        return VivoSignUtils.getVivoSign(params, appKey);
    }


    /**
     * 创建订单信息
     */
    public static VivoPayInfo createPayInfo(String appID, String appKey, String orderNumber, String extInfo, String notifyUrl, String orderAmount, String productName, String productDesc) {
        //步骤1：计算支付参数签名
        String signature = getSignature(appID, appKey, orderNumber, extInfo, notifyUrl, orderAmount, productName, productDesc);
        //步骤2：创建VivoPayInfo
        VivoPayInfo vivoPayInfo = new VivoPayInfo.Builder()
                //基本支付信息
                .setAppId(appID)
                .setCpOrderNo(orderNumber)
                .setExtInfo(extInfo)
                .setNotifyUrl(notifyUrl)
                .setOrderAmount(orderAmount)
                .setProductDesc(productDesc)
                .setProductName(productName)
                //计算出来的参数验签
                .setVivoSignature(signature)
                //无账号支付，传""
                .setExtUid("")
                .build();
        return vivoPayInfo;
    }

}
