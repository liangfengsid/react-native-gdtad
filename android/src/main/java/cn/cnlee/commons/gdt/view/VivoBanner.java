package cn.cnlee.commons.gdt.view;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.FrameLayout;

import com.vivo.mobilead.banner.BannerAdParams;
import com.vivo.mobilead.banner.VivoBannerAd;
import com.vivo.mobilead.listener.IAdListener;
import com.vivo.mobilead.model.BackUrlInfo;

import cn.cnlee.commons.gdt.R;


public class VivoBanner extends FrameLayout {

    private VivoBannerAd mBanner;
    private Runnable mLayoutRunnable;

    public VivoBanner(Context context, String posID, IAdListener listener) {
        this(context, null, posID, listener);
    }

    public VivoBanner(Context context, AttributeSet attrs, String posID, IAdListener listener) {
        this(context, attrs, 0, posID, listener);
    }

    public VivoBanner(Context context, AttributeSet attrs, int defStyleAttr, String posID, IAdListener listener) {
        super(context, attrs, defStyleAttr);
        // 把布局加载到这个View里面
        inflate(context, R.layout.layout_banner,this);
        initView(context, posID, listener);
    }

    /**
     * 初始化View
     */
    private void initView(Context context, String posID, IAdListener listener) {
        closeBanner();
        BannerAdParams.Builder builder = new BannerAdParams.Builder(posID);
        builder.setRefreshIntervalSeconds(15);
        BackUrlInfo backUrlInfo = new BackUrlInfo("", "");
        builder.setBackUrlInfo(backUrlInfo);
        mBanner = new VivoBannerAd((Activity) context, builder.build(), listener);
        addView(mBanner.getAdView());
    }

    public void closeBanner() {
        removeAllViews();
        if (mBanner != null) {
            mBanner.destroy();
            mBanner = null;
            Log.i("UnifiedBanner","关闭广告");
        }
        if (mLayoutRunnable != null){
            removeCallbacks(mLayoutRunnable);
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        closeBanner();
        super.onDetachedFromWindow();
    }

    @Override
    public void requestLayout() {
        super.requestLayout();
        if (mLayoutRunnable != null){
            removeCallbacks(mLayoutRunnable);
        }
        mLayoutRunnable = new Runnable() {
            @Override
            public void run() {
                measure(
                        MeasureSpec.makeMeasureSpec(getWidth(), MeasureSpec.EXACTLY),
                        MeasureSpec.makeMeasureSpec(getHeight(), MeasureSpec.EXACTLY));
                layout(getLeft(), getTop(), getRight(), getBottom());
            }
        };
        post(mLayoutRunnable);
    }

}
