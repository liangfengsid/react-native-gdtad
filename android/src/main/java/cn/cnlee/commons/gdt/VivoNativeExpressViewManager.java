package cn.cnlee.commons.gdt;

import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.common.MapBuilder;
import com.facebook.react.uimanager.SimpleViewManager;
import com.facebook.react.uimanager.ThemedReactContext;
import com.facebook.react.uimanager.annotations.ReactProp;
import com.facebook.react.uimanager.events.RCTEventEmitter;
import com.google.gson.Gson;

import com.vivo.ad.model.AdError;
import com.vivo.ad.nativead.NativeAdListener;
import com.vivo.ad.nativead.NativeResponse;
import com.vivo.mobilead.unified.base.VivoAdError;
import com.vivo.mobilead.unified.base.callback.MediaListener;

import java.util.List;
import java.util.Map;

import javax.annotation.Nullable;

import cn.cnlee.commons.gdt.view.VivoNativeExpress;

public class VivoNativeExpressViewManager extends SimpleViewManager implements NativeAdListener,MediaListener {

    private static final String TAG = "VivoNativeExpress";

    // 重写getName()方法, 返回的字符串就是RN中使用该组件的名称
    @Override
    public String getName() {
        return TAG;
    }

    @Override
    public void onADLoaded(List<NativeResponse> nativeResponses) {
        Log.i(TAG,"onADLoaded");
        // 释放前一个展示的NativeExpressADView的资源
        if (mNativeExpress != null) {
            mNativeExpress.closeNative();
        }

        if (nativeResponses != null && nativeResponses.size() > 0 && nativeResponses.get(0) != null) {
            NativeResponse response = nativeResponses.get(0);
            mNativeExpress.removeAllViews();
            if (response.getMaterialMode() == NativeResponse.MODE_VIDEO) {
                mNativeExpress.showVideo(response, this);
            } else if (response.getMaterialMode() == NativeResponse.MODE_UNKNOW) {   //展示无图片样式信息流广告
                mNativeExpress.showNoneImageAd(response);
            } else if (response.getMaterialMode() == NativeResponse.MODE_GROUP) {    //展示组图样式信息流广告
                mNativeExpress.showMultiImageAd(response);
            } else if (response.getMaterialMode() == NativeResponse.MODE_LARGE) {     //展示大图样式信息流广告
                mNativeExpress.showLargeImageAd(response);
            } else {
                mNativeExpress.showTinyImageAd(response);  //展示小图样式信息流广告
            }
        } else {
            Log.i(TAG, "NOADReturn");
            return;
        }
        if (mNativeExpress != null) {
            mNativeExpress.invalidate();
            mNativeExpress.requestLayout();
        }

        mEventEmitter.receiveEvent(mContainer.getId(), Events.EVENT_LOADED.toString(), null);
    }

    @Override
    public void onNoAD(AdError adError) {
        Log.i(TAG,"onNoAD: eCode=" + adError.getErrorCode() + ",eMsg=" + adError.getErrorMsg());
        WritableMap event = Arguments.createMap();
        event.putString("error", new Gson().toJson(adError));
        mEventEmitter.receiveEvent(mContainer.getId(), Events.EVENT_FAIL_TO_RECEIVED.toString(), event);
    }

    @Override
    public void onClick(NativeResponse nativeResponse) {
        Log.i(TAG,"onADClicked");
        mEventEmitter.receiveEvent(mContainer.getId(), Events.EVENT_ON_CLICK.toString(), null);
    }

    @Override
    public void onVideoStart() {
        Log.i(TAG, "onVideoStart");
    }

    @Override
    public void onVideoPause() {
        Log.i(TAG, "onVideoPause");
    }

    @Override
    public void onVideoPlay() {
        Log.i(TAG, "onVideoPlay");
    }

    @Override
    public void onVideoError(VivoAdError vivoAdError) {
        Log.i(TAG, "onVideoError");
    }

    @Override
    public void onVideoCompletion() {
        Log.i(TAG, "onVideoComplete");
    }

    public enum Events {
        EVENT_FAIL_TO_RECEIVED("onFailToReceived"),
        EVENT_LOADED("onLoaded"),
        EVENT_RENDER_FAIL("onFailed"),
        EVENT_RENDER_SUCCESS("onSucceeded"),
        EVENT_WILL_LEAVE_APP("onViewWillLeaveApplication"),
        EVENT_WILL_CLOSE("onViewWillClose"),
        EVENT_WILL_EXPOSURE("onViewWillExposure"),
        EVENT_ON_CLICK("onClicked"),
        EVENT_WILL_OPEN_FULL_SCREEN("onViewWillPresentFullScreenModal"),
        EVENT_DID_OPEN_FULL_SCREEN("onViewDidPresentFullScreenModal"),
        EVENT_WILL_CLOSE_FULL_SCREEN("onViewWillDismissFullScreenModal"),
        EVENT_DID_CLOSE_FULL_SCREEN("onViewDidDismissFullScreenModal");

        private final String mName;

        Events(final String name) {
            mName = name;
        }

        @Override
        public String toString() {
            return mName;
        }
    }

    private FrameLayout mContainer;
    private RCTEventEmitter mEventEmitter;
    private ThemedReactContext mThemedReactContext;
    private VivoNativeExpress mNativeExpress;

    @Override
    protected View createViewInstance(ThemedReactContext reactContext) {
        mThemedReactContext = reactContext;
        mEventEmitter = reactContext.getJSModule(RCTEventEmitter.class);
        FrameLayout container = new FrameLayout(reactContext);
        mContainer = container;
        return container;
    }

    @Nullable
    @Override
    public Map<String, Object> getExportedCustomDirectEventTypeConstants() {
        MapBuilder.Builder<String, Object> builder = MapBuilder.builder();
        for (Events event : Events.values()) {
            builder.put(event.toString(), MapBuilder.of("registrationName", event.toString()));
        }
        return builder.build();
    }

    // 其中，可以通过@ReactProp（或@ReactPropGroup）注解来导出属性的设置方法。
    // 该方法有两个参数，第一个参数是泛型View的实例对象，第二个参数是要设置的属性值。
    // 方法的返回值类型必须为void，而且访问控制必须被声明为public。
    // 组件的每一个属性的设置都会调用Java层被对应ReactProp注解的方法
    @ReactProp(name = "appInfo")
    public void setAppInfo(FrameLayout view, final ReadableMap appInfo) {
        String posID = appInfo.getString("posId");
        VivoNativeExpress nativ = new VivoNativeExpress(mThemedReactContext.getCurrentActivity(), posID, this);
        mNativeExpress = nativ;
        view.addView(mNativeExpress);
    }
}
