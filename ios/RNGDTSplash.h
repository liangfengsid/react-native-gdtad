//
//  RNGDTSplash.h
//  RNGdt
//
//  Created by Steven on 2017/6/14.
//  Copyright © 2017年 Facebook. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <React/RCTViewManager.h>

@interface RNGDTSplash: UIView
- (instancetype)initWithBackgroundImage;

 @property (nonatomic, strong) NSDictionary * appInfo;
 @property (nonatomic, assign) BOOL showLogo;

 @property (nonatomic, copy) RCTBubblingEventBlock onLoaded;
 @property (nonatomic, copy) RCTBubblingEventBlock onFailToReceived;
 @property (nonatomic, copy) RCTBubblingEventBlock onPresent;
 @property (nonatomic, copy) RCTBubblingEventBlock onSplashDismissed;
 @property (nonatomic, copy) RCTBubblingEventBlock onTick;
 @property (nonatomic, copy) RCTBubblingEventBlock onExposured;
 @property (nonatomic, copy) RCTBubblingEventBlock onClicked;

@end
