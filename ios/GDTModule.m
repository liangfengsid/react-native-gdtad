//
//  GDTModule.m
//  RNGdt
//
//  Created by liangfengsid on 2020/7/31.
//  Copyright © 2020 Qmaple. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "GDTModule.h"
#import "RewardVideo.h"
#import "GDTSDKConfig.h"
#import <React/RCTEventDispatcher.h>
#import <React/RCTBridge.h>
#import <React/RCTLog.h>
#import <React/RCTImageLoader.h>

@implementation GDTModule

//@synthesize bridge = _bridge;

RCT_EXPORT_MODULE()

RCT_EXPORT_METHOD(init:(NSString *)appid)
{
    BOOL result = [GDTSDKConfig registerAppId:appid];
    if (result) {
        NSLog(@"注册成功, appID: %@", appid);
    }
}

RCT_EXPORT_METHOD(setGDTPersonalizedOff:(NSInteger off)
{
    [GDTSDKConfig setPersonalizedState:off];
}

RCT_EXPORT_METHOD(showRewardVideoAD:(NSString *)posID
                  :(RCTResponseSenderBlock)onReward
                  )
{
    RewardVideo* rewardVideo = [[RewardVideo alloc] initWithCallBack:onReward];
    [rewardVideo showRewardVideoAD:posID];
}

@end
