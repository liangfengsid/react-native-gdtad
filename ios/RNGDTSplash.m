//
//  RNGDTSplash.m
//  RNGdt
//
//  Created by Steven on 2017/6/14.
//  Copyright © 2017年 Facebook. All rights reserved.
//

#import "RNGDTSplash.h"
#import "GDTSplashAd.h"

@interface RNGDTSplash() <GDTSplashAdDelegate>

@property (nonatomic, strong) GDTSplashAd *splashAd;
@property (nonatomic, strong) UIImage *backgroundImage;

@end

@implementation RNGDTSplash

- (instancetype)initWithBackgroundImage {
    self = [super init];
    _backgroundImage = [UIImage imageNamed:@"SplashNormal"];
    if (isIPhoneXSeries()) {
        _backgroundImage = [UIImage imageNamed:@"SplashX"];
    } else if ([UIScreen mainScreen].bounds.size.height == 480) {
        _backgroundImage = [UIImage imageNamed:@"SplashSmall"];
    }
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    CGFloat height = [UIScreen mainScreen].bounds.size.height;
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, width, height)];
    imgView.image = _backgroundImage;
    NSLog(@"DEBUG: splash background image: %@", _backgroundImage);
    if (self) {
        [self addSubview:imgView];
    }
    return self;
}

 - (void)setAppInfo:(NSDictionary *)appInfo {
     NSLog(@"in splash setAppInfo, posId: %@", appInfo[@"posId"]);
     if (self.splashAd) {
         self.splashAd.delegate = nil;
         self.splashAd = nil;
     }
     self.splashAd = [[GDTSplashAd alloc] initWithPlacementId:appInfo[@"posId"]];
     self.splashAd.delegate = self;
     self.splashAd.fetchDelay = 5;
     self.splashAd.backgroundImage = _backgroundImage;
     self.splashAd.backgroundImage.accessibilityIdentifier = @"splash_ad";
     [self.splashAd loadAd];
 }

- (void)setShowLogo:(BOOL)show {
    NSLog(@"in splash setShowLogo");
    _showLogo = show;
}

#pragma mark - GDTSplashAdDelegate

- (void)splashAdDidLoad:(GDTSplashAd *)splashAd {
    NSLog(@"%s, ecpmLevel:%@", __func__, splashAd.eCPMLevel);
    if (splashAd.isAdValid) {
        if (self.showLogo) {
            CGFloat logoHeight = [[UIScreen mainScreen] bounds].size.height * 0.15;
            UIView *bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, logoHeight)];
            bottomView.backgroundColor = [UIColor whiteColor];
            UIImageView *logo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"SplashLogo"]];
            logo.accessibilityIdentifier = @"splash_logo";
            logo.frame = CGRectMake(0, 0, 311, 47);
            logo.center = bottomView.center;
            [bottomView addSubview:logo];
            
            UIWindow *fK = [[UIApplication sharedApplication] keyWindow];
            [self.splashAd showAdInWindow:fK withBottomView:bottomView skipView:nil];
        } else {
            UIWindow *fK = [[UIApplication sharedApplication] keyWindow];
            [self.splashAd showAdInWindow:fK withBottomView:nil skipView:nil];
        }
    }
    
    if (self.onLoaded) {
        self.onLoaded(nil);
    }
}

- (void)splashAdSuccessPresentScreen:(GDTSplashAd *)splashAd
{
    NSLog(@"%s",__FUNCTION__);
    if (self.onPresent) {
        self.onPresent(nil);
    }
}

- (void)splashAdFailToPresent:(GDTSplashAd *)splashAd withError:(NSError *)error
{
    NSLog(@"%s%@",__FUNCTION__,error);
    if (self.onFailToReceived) {
        self.onFailToReceived(@{@"error": error.localizedDescription});
    }
}

- (void)splashAdExposured:(GDTSplashAd *)splashAd
{
    NSLog(@"%s",__FUNCTION__);
    if (self.onExposured) {
        self.onExposured(nil);
    }
}

- (void)splashAdClicked:(GDTSplashAd *)splashAd
{
    NSLog(@"%s",__FUNCTION__);
    if (self.onClicked) {
        self.onClicked(nil);
    }
}

- (void)splashAdApplicationWillEnterBackground:(GDTSplashAd *)splashAd
{
    NSLog(@"%s",__FUNCTION__);
}

- (void)splashAdWillClosed:(GDTSplashAd *)splashAd
{
    NSLog(@"%s",__FUNCTION__);
}

- (void)splashAdClosed:(GDTSplashAd *)splashAd
{
    NSLog(@"%s",__FUNCTION__);
    self.splashAd = nil;
    if (self.onSplashDismissed) {
        self.onSplashDismissed(nil);
    }
}

- (void)splashAdWillPresentFullScreenModal:(GDTSplashAd *)splashAd
{
    NSLog(@"%s",__FUNCTION__);
}

- (void)splashAdDidPresentFullScreenModal:(GDTSplashAd *)splashAd
{
    NSLog(@"%s",__FUNCTION__);
}

- (void)splashAdWillDismissFullScreenModal:(GDTSplashAd *)splashAd
{
    NSLog(@"%s",__FUNCTION__);
}

- (void)splashAdDidDismissFullScreenModal:(GDTSplashAd *)splashAd
{
    NSLog(@"%s",__FUNCTION__);
}

@end
