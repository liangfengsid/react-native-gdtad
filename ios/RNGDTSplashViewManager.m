//
//  RNGDTSplashViewManager.m
//  RNGdt
//
//  Created by Steven on 2017/6/14.
//  Copyright © 2017年 Facebook. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RNGDTSplashViewManager.h"
#import "RNGDTSplash.h"

@implementation RNGDTSplashViewManager

// - (dispatch_queue_t)methodQueue {
//     return dispatch_get_main_queue();
// }

 RCT_EXPORT_MODULE(GDTSplash);

 RCT_EXPORT_VIEW_PROPERTY(appInfo, NSDictionary);
 RCT_EXPORT_VIEW_PROPERTY(showLogo, BOOL);

 RCT_EXPORT_VIEW_PROPERTY(onLoaded, RCTBubblingEventBlock);
 RCT_EXPORT_VIEW_PROPERTY(onFailToReceived, RCTBubblingEventBlock);
 RCT_EXPORT_VIEW_PROPERTY(onPresent, RCTBubblingEventBlock);
 RCT_EXPORT_VIEW_PROPERTY(onSplashDismissed, RCTBubblingEventBlock);
 RCT_EXPORT_VIEW_PROPERTY(onTick, RCTBubblingEventBlock);
 RCT_EXPORT_VIEW_PROPERTY(onExposured, RCTBubblingEventBlock);
 RCT_EXPORT_VIEW_PROPERTY(onClicked, RCTBubblingEventBlock);

 - (UIView *)view {
     return [[RNGDTSplash alloc] initWithBackgroundImage];
 }

@end
